package main

import (
	"bufio"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"os/exec"
)

func main() {
	port := flag.Int("p", 52698, "Port to listen")
	flag.Parse()
	ln, err := net.Listen("tcp", fmt.Sprintf("localhost:%d", *port))
	if err != nil {
		log.Fatal(err)
	}
	for {
		if err := handleConn(ln); err != nil {
			log.Println(err)
			continue
		}
	}
}

func handleConn(ln net.Listener) error {
	conn, err := ln.Accept()
	if err != nil {
		return err
	}
	defer conn.Close()
	r := bufio.NewReader(conn)
	message, err := r.ReadBytes('\n')
	cmd := string(message[:len(message)-1])
	if err != nil {
		return err
	}
	if cmd == "put" {
		body, _ := ioutil.ReadAll(r)
		if err := put(body); err != nil {
			return err
		}
	} else if cmd == "get" {
		body, err := get()
		if err != nil {
			return err
		}
		w := bufio.NewWriter(conn)
		if _, err := w.Write(body); err != nil {
			return err
		}
		if err := w.Flush(); err != nil {
			return err
		}
	} else {
		return fmt.Errorf("Unknown command: \"" + cmd + "\"")
	}
	return nil
}

func put(message []byte) error {
	cmd := exec.Command("pbcopy")
	in, err := cmd.StdinPipe()
	if err != nil {
		return err
	}
	if err := cmd.Start(); err != nil {
		return err
	}
	if _, err := in.Write(message); err != nil {
		return err
	}
	if err := in.Close(); err != nil {
		return err
	}
	return cmd.Wait()
}

func get() ([]byte, error) {
	cmd := exec.Command("pbpaste")
	out, err := cmd.Output()
	if err != nil {
		return nil, err
	}
	return []byte(out), nil
}
